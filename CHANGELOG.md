# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.2](https://gitee.com/haijunit/element-admin/compare/v1.3.1...v2.0.2) (2024-04-29)

### [1.3.1](https://gitee.com:haijunit/element-admin/compare/v1.3.0...v1.3.1) (2024-04-29)
